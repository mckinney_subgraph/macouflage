#!/usr/bin/env python
# -*- coding: utf-8 -*-

from pymacouflage.api import macouflagedbusclient as client
from gi.repository import Gtk
from pymacouflage.api import api as mf
from pymacouflage.api import macutility as mu
import sys

# TODO: Monitor NM "device added" to figure out when new devices are added and refresh device list
# TODO: Also look at udev - https://wiki.gnome.org/Projects/PyGObject/IntrospectionPorting

class MacouflageWindow(Gtk.Window):
    def __init__(self):
        self.dbusclient = client.MacouflageDbusClient()
        self.macouflage = mf.Macouflage()
        self.selected_device = None

        Gtk.Window.__init__(self, title="Macouflage")
        self.set_position(Gtk.WindowPosition.CENTER)
        self.set_size_request(640, 480)
        self.set_border_width(5)
        self.hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        self.vbox = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        self.add(self.hbox)
        self.hbox.pack_end(self.vbox, True, True, 0)


        self.treeview = Gtk.TreeView()
        self.treeview.set_grid_lines(Gtk.TreeViewGridLines.VERTICAL)
        self.textrenderer = Gtk.CellRendererText()
        self.togglerenderer = Gtk.CellRendererToggle()
        self.pixbufrenderer = Gtk.CellRendererPixbuf()
        self.togglerenderer.set_radio(False)
        self.togglerenderer.set_activatable(True)
        column1 = Gtk.TreeViewColumn("Devices", self.textrenderer, text=0)
        column1.set_min_width(75)
        column2 = Gtk.TreeViewColumn("Spoofed MAC", self.pixbufrenderer, stock_id=1)
        self.treeview.append_column(column1)
        self.treeview.append_column(column2)
        self.liststore = Gtk.ListStore(str, str)
        self.hbox.pack_start(self.treeview, False, True, 0)

        self.update_model()
        self.treeview.set_model(self.liststore)

        self.grid = Gtk.Grid()
        self.device_label = Gtk.Label("", xalign=0)
        self.permanent_mac_label = Gtk.Label("Permanent MAC", xalign=0)
        self.permanent_mac_label.set_width_chars(15)
        self.permanent_mac_label_content = Gtk.Label("", xalign=0)
        self.permanent_mac_label_content.set_width_chars(20)
        self.current_mac_label = Gtk.Label("Current MAC", xalign=0)
        self.current_mac_label.set_width_chars(15)
        self.current_mac_label_content = Gtk.Label("", xalign=0)
        self.current_mac_label_content.set_width_chars(20)
        self.grid.add(self.device_label)
        self.grid.attach(self.permanent_mac_label, 0, 1, 1, 1)
        self.grid.attach(self.permanent_mac_label_content, 1, 1, 1, 1)
        self.grid.attach(self.current_mac_label, 0, 2, 1, 1)
        self.grid.attach(self.current_mac_label_content, 1, 2, 1, 1)
        self.vbox.pack_start(self.grid, True, True, 0)

        self.options_liststore = Gtk.ListStore(str)
        for k,v in self.macouflage.options.iteritems():
            self.options_liststore.append([v])
        cell = Gtk.CellRendererText()
        self.options_combobox = Gtk.ComboBox.new_with_model(self.options_liststore)
        self.options_combobox.pack_start(cell, True)
        self.vbox.pack_start(self.options_combobox, False, False, 0)
        self.options_combobox.add_attribute(cell, "text", 0)
        self.options_combobox.set_active(0)
        self.options_combobox.connect("changed", self.on_options_combobox_changed)

        self.revealer1 = Gtk.Revealer()
        self.vbox.pack_start(self.revealer1, False, False, 0)
        self.revealer1.set_transition_type(Gtk.RevealerTransitionType.CROSSFADE)
        self.mac_entry_grid = Gtk.Grid(column_homogeneous=False)
        self.mac_entry_grid.set_column_spacing(6)
        self.mac_entry_grid.set_row_spacing(6)
        self.mac_entry_header_label = Gtk.Label("MAC address: ")
        self.mac_entry0 = Gtk.Entry()
        self.mac_entry0.set_max_length(2)
        self.mac_entry0.set_width_chars(2)
        self.mac_entry0.connect("changed", self.on_mac_entry_changed)
        self.mac_entry0_label = Gtk.Label(":")
        self.mac_entry1 = Gtk.Entry()
        self.mac_entry1.set_max_length(2)
        self.mac_entry1.set_width_chars(2)
        self.mac_entry1.connect("changed", self.on_mac_entry_changed)
        self.mac_entry1_label = Gtk.Label(":")
        self.mac_entry2 = Gtk.Entry()
        self.mac_entry2.set_max_length(2)
        self.mac_entry2.set_width_chars(2)
        self.mac_entry2.connect("changed", self.on_mac_entry_changed)
        self.mac_entry2_label = Gtk.Label(":")
        self.mac_entry3 = Gtk.Entry()
        self.mac_entry3.set_max_length(2)
        self.mac_entry3.set_width_chars(2)
        self.mac_entry3.connect("changed", self.on_mac_entry_changed)
        self.mac_entry3_label = Gtk.Label(":")
        self.mac_entry4 = Gtk.Entry()
        self.mac_entry4.set_max_length(2)
        self.mac_entry4.set_width_chars(2)
        self.mac_entry4.connect("changed", self.on_mac_entry_changed)
        self.mac_entry4_label = Gtk.Label(":")
        self.mac_entry5 = Gtk.Entry()
        self.mac_entry5.set_max_length(2)
        self.mac_entry5.set_width_chars(2)
        self.mac_entry5.connect("changed", self.on_mac_entry_changed)
        self.mac_entry_grid.attach(self.mac_entry_header_label, 0, 0, 3, 1)
        self.mac_entry_grid.attach(self.mac_entry0, 0, 1, 1, 1)
        self.mac_entry_grid.attach(self.mac_entry0_label, 1, 1, 1, 1)
        self.mac_entry_grid.attach(self.mac_entry1, 2, 1, 1, 1)
        self.mac_entry_grid.attach(self.mac_entry1_label, 3, 1, 1, 1)
        self.mac_entry_grid.attach(self.mac_entry2, 4, 1, 1, 1)
        self.mac_entry_grid.attach(self.mac_entry2_label, 5, 1, 1, 1)
        self.mac_entry_grid.attach(self.mac_entry3, 6, 1, 1, 1)
        self.mac_entry_grid.attach(self.mac_entry3_label, 7, 1, 1, 1)
        self.mac_entry_grid.attach(self.mac_entry4, 8, 1, 1, 1)
        self.mac_entry_grid.attach(self.mac_entry4_label, 9, 1, 1, 1)
        self.mac_entry_grid.attach(self.mac_entry5, 10, 1, 1, 1)
        self.revealer1.add(self.mac_entry_grid)

        self.buttonbox = Gtk.ButtonBox(spacing = 20)
        self.buttonbox.set_layout(Gtk.ButtonBoxStyle.END)
        self.spoof_button = Gtk.Button("Spoof MAC")
        self.revert_button = Gtk.Button("Revert to permanent MAC")
        self.buttonbox.add(self.spoof_button)
        self.buttonbox.add(self.revert_button)
        self.vbox.pack_start(self.buttonbox, True, True, 0)

        self.infobar = Gtk.InfoBar()
        self.infobar_label = Gtk.Label("", xalign = 0)
        self.infobar_content = self.infobar.get_content_area()
        self.infobar_content.add(self.infobar_label)
        self.vbox.pack_start(self.infobar, False, True, 0)
        self.infobar.add_button(Gtk.STOCK_CLOSE, Gtk.ResponseType.CLOSE)
        self.infobar.set_default_response(Gtk.ResponseType.CLOSE)
        self.infobar.connect("response", self.infobar_response)

        self.selection = self.treeview.get_selection()
        self.selection.set_mode(Gtk.SelectionMode.BROWSE)
        self.selection.connect("changed", self.on_tree_selection_changed)

        self.spoof_button.connect("clicked", self.spoof_mac)
        self.revert_button.connect("clicked", self.revert_mac)

        self.show_all()
        self.infobar.hide()

    def on_tree_selection_changed(self, selection):
        device = self.get_selected_device(selection)
        self.selected_device = device
        self.update_device_information(device)
        return device

    def get_selected_device(self, selection):
        model, tree_iter = selection.get_selected()
        if tree_iter != None:
            device = model[tree_iter][0]
            return device
        else:
            return None

    def on_options_combobox_changed(self, widget):
        self.spoof_button.set_sensitive(True)
        self.revealer1.set_reveal_child(False)
        model = self.options_combobox.get_model()
        index = self.options_combobox.get_active()
        if index == 4:
            self.revealer1.set_reveal_child(True)
            self.validate_mac()

    def on_mac_entry_changed(self, widget):
        self.validate_mac()

    # TODO: Consider tracking "ifup/ifdown" in here, may be annoying to update through
    def update_device_information(self, device_name):
        self.device_label.set_text("Interface {0}".format(device_name))
        current_mac = self.macouflage.get_current_mac_by_device_with_vendor(device_name)
        permanent_mac = self.macouflage.get_permanent_mac_by_device_with_vendor(device_name)
        self.permanent_mac_label_content.set_text(permanent_mac)
        self.current_mac_label_content.set_text(current_mac)
        Gtk.Widget.show_all(self.grid)

    def update_model(self):
        interfaces = self.dbusclient.list_interfaces()
        for iface in interfaces:
            if not self.macouflage.find_interface_by_device(iface):
                self.macouflage.add_interface(iface)
                self.liststore.append([iface, self.choose_stock(self.macouflage.is_mac_spoofed(iface))])
            else:
                row = self.find_device_in_model(iface)
                self.liststore.set(row, 0, iface, 1, self.choose_stock(self.macouflage.is_mac_spoofed(iface)))

    def choose_stock(self, state):
        if state:
            return Gtk.STOCK_APPLY
        else:
            return Gtk.STOCK_CANCEL

    def infobar_changed(self, button, message_type, message):
        self.infobar_label.set_text(message)
        self.infobar.set_message_type(message_type)
        self.infobar.show()

    def infobar_response(self, infobar, response_id):
        self.infobar.hide()

    def find_device_in_model(self, device_name):
        return ([r.iter for r in self.liststore if r[0] == device_name ])[0]

    def validate_mac(self):
        mac_string = "{0}:{1}:{2}:{3}:{4}:{5}".format(self.mac_entry0.get_text(),
                                               self.mac_entry1.get_text(),
                                               self.mac_entry2.get_text(),
                                               self.mac_entry3.get_text(),
                                               self.mac_entry4.get_text(),
                                               self.mac_entry5.get_text())
        self.spoof_button.set_sensitive(mu.MacUtility().validate_mac(mac_string))

    def spoof_mac(self, widget):
        device_name = self.get_selected_device(self.treeview.get_selection())
        index = self.options_combobox.get_active()
        option = self.macouflage.options.items()[index][0]
         
        try:
            if index == 4:
                mac_string = "{0}:{1}:{2}:{3}:{4}:{5}".format(self.mac_entry0.get_text(),
                                               self.mac_entry1.get_text(),
                                               self.mac_entry2.get_text(),
                                               self.mac_entry3.get_text(),
                                               self.mac_entry4.get_text(),
                                               self.mac_entry5.get_text())

                result = self.macouflage.spoof_mac(device_name, option, base_mac = mac_string)
                
            else:
                result = self.macouflage.spoof_mac(device_name, option)
            if result:
                self.update_model()
                self.update_device_information(device_name)
                self.infobar_changed(Gtk.ResponseType.OK, Gtk.MessageType.INFO, "MAC changed successfully")
            else:
                response = "Unable to change MAC address.\nProblems:\n{0}".format("\n".join(self.macouflage.reasons_for_not_working(device_name)))
                self.infobar_changed(Gtk.ResponseType.OK, Gtk.MessageType.ERROR, response)
        except IOError as detail:
            print("Exception: {0}".format(detail))
            pass

    def revert_mac(self, widget):
        device_name = self.get_selected_device(self.treeview.get_selection())
        try:
            if self.macouflage.revert_mac(device_name):
                self.update_model()
                self.update_device_information(device_name)
                self.infobar_changed(Gtk.ResponseType.OK, Gtk.MessageType.INFO, "MAC successfully reverted")
            else:
                response = "Unable to revert MAC address.\nProblems:\n{0}".format("\n".join(self.macouflage.reasons_for_not_working(device_name)))
                self.infobar_changed(Gtk.ResponseType.OK, Gtk.MessageType.ERROR, response)
        except IOError as detail:
            print("Exception: {0}".format(detail))

    def start_gui(self):
        self.connect("delete-event", Gtk.main_quit)
        import signal
        signal.signal(signal.SIGINT, signal.SIG_DFL)
        Gtk.main()

if __name__ == '__main__':
    print("GTK GUI must be invoked from macouflage command-line utility.")
    sys.exit(1)
