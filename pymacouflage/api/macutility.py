import re
class MacUtility(object):
    @staticmethod
    def mac_array_to_string(mac_array):
        return ':'.join('%02x' % b for b in mac_array)

    @staticmethod
    def mac_string_to_array(mac_string):
        return [ord(s.decode('hex')) for s in mac_string.split(':')]

    @staticmethod
    def validate_mac(mac_string):
        try:
            re.search(r'([0-9A-F]{2}[:-]){5}([0-9A-F]{2})', mac_string, re.I).group()
        except AttributeError:
            return False
        return True

