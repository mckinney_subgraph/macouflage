#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import re
import random
import os

class OuiDb(object):
    def __init__(self):
        data_path = os.path.dirname(os.path.abspath(__file__))
        self.entries = []
        with open(os.path.join(data_path, 'data/ouis.json')) as data_file:
            self.entries = json.load(data_file) 
    
    def find_by_vendor_name(self, vendor_name):
        regex = re.compile(".*{0}.*".format(vendor_name), re.IGNORECASE)
        return [x for x in self.entries if regex.match(x['vendor_name'])]
                                                                                
    def find_by_vendor_prefix(self, vendor_prefix):                           
        return [x for x in self.entries if x['vendor_prefix'] == vendor_prefix.upper()][0]
                                                                                
    def find_by_popular(self, is_popular = True):
        return [x for x in self.entries if x['is_popular'] == is_popular]

    def find_by_random(self, is_popular = False):
        if is_popular:
            return random.choice(self.find_by_popular())
        else:
            return random.choice(self.entries)

    def find_by_device_type(self, device_type):
        regex = re.compile(".*{0}.*".format(device_type), re.IGNORECASE)        
        return [x for x in self.entries for y in x['devices'] if regex.match(y['device_type'])]
    
    def find_by_device_name(self, device_name):                                 
        regex = re.compile(".*{0}.*".format(device_name), re.IGNORECASE)        
        return [x for x in self.entries for y in x['devices'] if regex.match(y['device_name'])]

    def find_by_mac(self, mac):
        return [x for x in self.entries if x['vendor_prefix'] == mac[0:8].upper()][0]
  
    def list_unique_device_types(self):
        device_types = []
        for entry in self.entries:
            for device in entry['devices']:
                device_types.append(device['device_type'])
        return sorted(set(device_types))

    def list_unique_device_names(self):
        device_names = []
        for entry in self.entries:
            for device in entry['devices']:
                device_names.append(device['device_name'])
        return sorted(set(device_names))
    
