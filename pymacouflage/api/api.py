#!/usr/bin/env python
# -*- coding: utf-8 -*-

import random
from networkinterface import NetworkInterface
from ouidb import OuiDb
from macgenerator import MacGenerator
from macutility import MacUtility

class Macouflage(object):
    def __init__(self):
        self.interfaces = {}
        macgen = MacGenerator()
        self.options = macgen.options
        self.ouidb = OuiDb()

    def add_interface(self, device_name):
        if device_name not in self.interfaces:
            self.interfaces[device_name] = NetworkInterface(device_name)

    def get_all_current_macs(self):
        results = {}
        for iface in self.interfaces:
            mac = iface.get_mac()
            results[iface.device_name] = mac
        return results

    def get_current_mac_by_device(self, device_name):
        iface = self.find_interface_by_device(device_name)
        return iface.get_mac()

    def get_current_mac_by_device_with_vendor(self, device_name):
        iface = self.find_interface_by_device(device_name)
        mac = iface.get_mac()
        return "{0} ({1})".format(mac, self.find_vendor_by_mac(mac))            

    def get_all_permanent_macs(self):
        results = {}
        for iface in self.interfaces:
            mac = iface.get_permanent_mac()
            results[iface.iface.device_name] = mac
        return results

    def get_permanent_mac_by_device(self, device_name):
        iface = self.find_interface_by_device(device_name)
        return iface.get_permanent_mac()

    def get_permanent_mac_by_device_with_vendor(self, device_name):
        iface = self.find_interface_by_device(device_name)
        mac = iface.get_permanent_mac()
        return "{0} ({1})".format(mac, self.find_vendor_by_mac(mac))

    def is_mac_spoofed(self, device_name):
        iface = self.find_interface_by_device(device_name)
        return iface.mac_changed()

    def find_interface_by_device(self, device_name):
        if device_name in self.interfaces:
            return self.interfaces[device_name]
        else:
            return None

    def find_random_vendor_by_device_type(self, device_type):
        return random.choice(self.ouidb.find_by_device_type(device_type))['vendor_prefix']
    
    def find_vendor_by_mac(self, mac):
        return self.ouidb.find_by_mac(mac)['vendor_name']

    def spoof_mac(self, device_name, option, base_mac="", force=False, is_bia=False):
        iface = self.find_interface_by_device(device_name)
        if iface:
            if force:
                self.ifdown(device_name)
            if self.options[option]:
                if option == "SPECIFIC":
                    base_mac = base_mac
                elif option == "POPULAR":
                    base_mac = MacUtility.mac_string_to_array("{0}:00:00:00".format(self.ouidb.find_by_random(is_popular = True)['vendor_prefix']))
                    is_bia = True
                elif option == "SAMEKIND":
                    device_type = self.ouidb.find_by_mac(iface.get_mac())['devices'][0]['device_type']
                    vendor_prefix = self.find_random_vendor_by_device_type(device_type)
                    base_mac = MacUtility.mac_string_to_array("{0}:00:00:00".format(vendor_prefix))
                    is_bia = True
                elif option == "ANY":
                    current_mac = iface.get_mac()
                    base_mac = "{0}{1}".format(self.ouidb.find_by_random(is_popular = False)['vendor_prefix'], current_mac[8:17])
                    is_bia = True
                else:
                    base_mac = MacUtility.mac_string_to_array(iface.get_mac())
                    is_bia = is_bia
                macgen = MacGenerator(option=option, base_mac=base_mac, is_bia=is_bia)
                new_mac = macgen.generate()
                iface.set_mac(new_mac)
                current_mac = iface.get_mac()
                if self.is_mac_spoofed(device_name) and current_mac == new_mac:
                    if force:
                        self.ifup(device_name)
                    return True
                else:
                    return False
            else:
                raise KeyError("{0} is not in the list of spoofing options.".format(option))
        else:
            raise KeyError("{0} is not in the list of devices".format(device_name))

    def revert_mac(self, device_name, force=False):
        iface = self.find_interface_by_device(device_name)
        if iface:
            if iface.mac_changed():
                permanent_mac = iface.get_permanent_mac()
                iface.set_mac(permanent_mac)
                if iface.mac_changed():
                    return False
                else:
                    return True
            else:
                return False
        else:
            raise KeyError("{0} is not in the list of devices".format(device_name))

    def reasons_for_not_working(self, device_name):
        iface = self.find_interface_by_device(device_name)
        return iface.reasons_for_not_working()

    def ifdown(self, device_name):
        iface = self.find_interface_by_device(device_name)
        iface.ifdown()
    def ifup(self, device_name):
        iface = self.find_interface_by_device(device_name)
        iface.ifup()



