#!/usr/bin/env python
# -*- coding: utf-8 -*-

from fcntl import ioctl
import os
import sys
import array
import socket
import struct
import binascii
import re
import subprocess
from macutility import MacUtility


SIOCSIFHWADDR = 0x8924
SIOCGIFHWADDR = 0x8927
SIOCETHTOOL = 0x8946
ETHTOOL_GPERMADDR = 0x00000020
ETHTOOL_GDRVINFO = 0x00000003
AF_UNIX = 1
IFHWADDRLEN = 6

class NetworkInterface(object):
    def __init__(self, device_name, sockfd = None, res = None):
        self.device_name = device_name
        self.sockfd = socket.socket()


    def get_mac(self):
        ifreq = struct.pack('16sH14s', self.device_name, AF_UNIX, '\x00'*14)
        res = ioctl(self.sockfd, SIOCGIFHWADDR, ifreq)
        address = struct.unpack('16sH14s', res)[2]
        return self.unpack_mac(address)

    def set_mac(self, mac):
        if MacUtility.validate_mac(mac):
            macbytes = [int(i, 16) for i in mac.split(':')]
            ifreq = struct.pack('16sH6B8x', self.device_name, AF_UNIX, *macbytes)
            try:
                ioctl(self.sockfd, SIOCSIFHWADDR, ifreq)
            except IOError as detail:

                print("[ERROR] Could not change MAC: interface up or insufficient permissions: %s" % detail.strerror)
                #sys.exit(1)
            return True
        else:
            print("Supplied MAC address is invalid: %s" % mac)
            return False

    def get_permanent_mac(self):
        epa = array.array('B', struct.pack('2I6s', ETHTOOL_GPERMADDR, IFHWADDRLEN, '\x00'*6))
        ifreq = struct.pack('16sP', self.device_name, epa.buffer_info()[0])
        try:
            ioctl(self.sockfd, SIOCETHTOOL, ifreq)
        except IOError as detail:
            print("[ERROR] Could not get interface permanent MAC address for {0}: {1}" .format(self.device_name, detail.strerror))
        #    sys.exit(1)
        
        address = struct.unpack('2I6s', epa)[2]
        return self.unpack_mac(address)

    def unpack_mac(self,address):
        mac = binascii.hexlify(address)
        return  mac[0:2] + ":" + mac[2:4] + ":" + mac[4:6] + ":" + mac[6:8] + ":" + mac[8:10] + ":" + mac[10:12]

    def compare_macs(self, first_mac, second_mac):
        return first_mac == second_mac

    def mac_changed(self):
        if  not self.compare_macs(self.get_permanent_mac(), self.get_mac()):
            return True
        else:
            return False

    def reasons_for_not_working(self):
        reasons = []
        if os.geteuid() != 0:
            reasons.append("You need to run as root!")
        if self.is_ifup:
            reasons.append("You should bring {0} down before spoofing the MAC!".format(self.device_name))
        return reasons

    def ifdown(self):
        if self.is_ifup:
            result = subprocess.check_call(["/bin/ip", "link", "set", "down", self.device_name])
            return result
        else:
            return False

    def ifup(self):
        if self.is_ifdown:
            result = subprocess.check_call(["/bin/ip", "link", "set", "up", self.device_name])
            return result
        else:
            return False

    def is_ifdown(self):
        output = subprocess.check_output(["/bin/ip", "link", "show", self.device_name])
        regex = re.compile("^.+{0}.+state.+DOWN.+".format(self.device_name))
        return regex.match(output)

    def is_ifup(self):
        output = subprocess.check_output(["/bin/ip", "link", "show", self.device_name])
        regex = re.compile("^.+{0}.+state.+UP.+".format(self.device_name))
        return regex.match(output)


