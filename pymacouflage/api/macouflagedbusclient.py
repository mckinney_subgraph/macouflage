#!/usr/bin/env python
# -*- coding: utf-8 -*-

import dbus

# Very important page: https://developer.gnome.org/NetworkManager/unstable/spec.html
class MacouflageDbusClient(object):

    def __init__(self):
        self.bus = dbus.SystemBus()

    # qdbus --literal --system org.freedesktop.NetworkManager /org/freedesktop/NetworkManager org.freedesktop.DBus.Properties.Get org.freedesktop.NetworkManager ActiveConnections
    def list_interfaces(self):
        device_names = []
        proxy = self.bus.get_object('org.freedesktop.NetworkManager', '/org/freedesktop/NetworkManager')
        iface = dbus.Interface(proxy, 'org.freedesktop.NetworkManager')
        devices =  iface.GetDevices()
        for device in devices:
            device_proxy = self.bus.get_object('org.freedesktop.NetworkManager', device)
            properties_manager = dbus.Interface(device_proxy, 'org.freedesktop.DBus.Properties')
            device_type = properties_manager.Get('org.freedesktop.NetworkManager.Device', 'DeviceType')
            
            if device_type == 14:
                continue

            device_name = properties_manager.Get('org.freedesktop.NetworkManager.Device', 'Interface')
            device_names.append(str(device_name))
        return device_names
        # TODO: Need to know if it is connected, should return a device object

    def get_device(self, device_name):
        # qdbus --literal --system org.freedesktop.NetworkManager /org/freedesktop/NetworkManager GetDeviceByIpIface wlan0
        proxy = self.bus.get_object('org.freedesktop.NetworkManager', '/org/freedesktop/NetworkManager')
        iface = dbus.Interface(proxy, 'org.freedesktop.NetworkManager')
        device_path = iface.GetDeviceByIpIface(device_name)

        return device_path

    # TODO: Implement NetworkManager methods
    def ifdwn(self, device_name):
        pass
    def ifup(self, device_name):
        pass

client = MacouflageDbusClient()



