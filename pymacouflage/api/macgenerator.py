#!/usr/bin/env python
# -*- coding: utf-8 -*-

import string
import os
from collections import OrderedDict
from macutility import MacUtility

class MacGenerator(object):

    def __init__(self, option = 'RANDOM', is_bia = True, base_mac = [0x00, 0x00, 0x00, 0x00, 0x00, 0x00]):
        self.option = option
        self.is_bia = is_bia
        self.base_mac = base_mac
        self._options = OrderedDict()
        self._options['RANDOM'] = 'Completely random MAC'
        self._options['SAMEVENDOR'] = 'Keep vendor, change ending'
        self._options['SAMEKIND'] = 'Random MAC of same device type'
        self._options['ANY'] = 'Any device type'
        self._options['SPECIFIC'] = 'Set a specific MAC'
        self._options['POPULAR'] = 'Random MAC from a popular vendor'

    def generate(self):
        if self._options[self.option]:
            if self.option == 'ANY':
                return self.generate_any()
            elif self.option == 'RANDOM':
                return self.generate_random()
            elif self.option == 'SAMEVENDOR':
                return self.generate_same_vendor()
            elif self.option == 'SAMEKIND':
                return self.generate_same_kind()
            elif self.option == 'ANY':
                return self.generate_any()
            elif self.option == 'SPECIFIC':
                return self.generate_specific()
            elif self.option == 'POPULAR':
                return self.generate_popular()
        else:
            return None

    def generate_any(self):
        return self.base_mac

    def generate_random(self):
        mac = MacUtility.mac_array_to_string(self.mac_randomize(self.base_mac))
        return mac

    def generate_same_vendor(self):
        return MacUtility.mac_array_to_string(self.mac_randomize(self.base_mac, start=3, end=6))

    def generate_specific(self):
        return self.base_mac

    # TODO: kinds should use NetworkManager types
    # Name:	getType			Returns the type of the device (ie wired, wireless, isdn, bluetooth, etc)
    #	Args:	(none)
    #   Returns:	DBUS_TYPE_INT32			0 - unknown type
    #							1 - Wired ethernet
    #							2 - Wireless (802.11a/b/g)
    # http://people.redhat.com/dcbw/NetworkManager/NetworkManager%20DBUS%20API.txt
    # https://projects-old.gnome.org/NetworkManager/developers/api/08/spec-08.html
    # JSON OUI database should also use these types
    # SAME KIND will be NM type, not "others" or "wireless" -- though these options will be exposed via macchanger cli

    def generate_same_kind(self):
        return MacUtility.mac_array_to_string(self.mac_randomize(self.base_mac, start=3, end=6))

    def generate_popular(self):
        return MacUtility.mac_array_to_string(self.mac_randomize(self.base_mac, start=3, end=6))

    def mac_randomize(self, mac_array, start = 0, end = 6):
        for i in range(start, end):
            if i == 0:
                mac_array[i] =  int(os.urandom(1).encode('hex'), 16) & 0xfc
            else:
                mac_array[i] =  int(os.urandom(1).encode('hex'), 16)
        
        if self.is_bia:
            mac_array[0] &= ~2
        else:
            mac_array[0] |= 2
        return mac_array

    @property
    def options(self):
        return self._options

