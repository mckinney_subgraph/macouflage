# Macouflage
Macouflage is a MAC address anonymization tool similar in functionality to
[GNU Mac Changer](http://directory.fsf.org/wiki/GNU_MAC_Changer). The main
difference is that Macouflage supports additional modes of operation such as
generating spoofed MAC addresses from a list of popular network devices.

# Data sources

Macouflage ships with a JSON database derived from IEEE's OUI 
(Organizationally Unique Identifier)/MA-L (MAC Address Block Large) [registry](http://standards.ieee.org/develop/regauth/oui/oui.txt).

The popular mode currently uses the data gathered  by Etienne Perot's
[macchiato](https://github.com/EtiennePerot/macchiato) project. 

The data is merged from these data sources by the supplementary [ouiner](https://bitbucket.org/mckinney_subgraph/ouiner) 
project.

# Usage
```
macouflage.py -h                                                                                                                                                                                              -- INSERT --
usage: macouflage.py [-h] [-V] [-l LIST] [-r] [-e] [-b BIA] [-a] [-s] [-A]
                     [-p] [-P] [-f] [-m MAC] [-c] [-g]
                     [device]

positional arguments:
  device

optional arguments:
  -h, --help            show this help message and exit
  -V, --version         print version and exit
  -l LIST, --list LIST  print known vendors
  -r, --random          set fully random MAC
  -e, --ending          don't change vendor bytes
  -b BIA, --bia BIA     pretend to be a burned-in address
  -a, --another         set random vendor MAC of the same kind
  -s, --show            print the MAC address and exit
  -A, --any             set random vendor MAC of any kind
  -p, --permanent       reset to original, permanent vendor MAC
  -P, --popular         set MAC to one used by a popular vendor
  -f, --force           Force MAC change (bring interface down to change MAC)
  -m MAC, --mac MAC     set the MAC XX:XX:XX:XX:XX:XX
  -c, --check           check if device is spoofed and exit
  -g, --gui             start macouflage gui
```

# Examples

## Popular mode
```
$ sudo macouflage.py -P wlan0                                                                                                                                                                                   -- INSERT --
Current MAC:   (Some MAC) (Some vendor))
Permanent MAC: (Some MAC) (Some vendor)
New MAC: 40:2c:f4:e8:45:90
```

## Is my interface spoofed?
```
$ macouflage.py -c wlan0                                                                                                                                                                                        -- INSERT --
wlan0 is spoofed
```

## Start GUI
```
$ macouflage.py -g
```

