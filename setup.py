#!/usr/bin/env python
"""
Macouflage
https://github.com/subgraph/macouflage/

Copyright (c) 2014 David McKinney/Subgraph <info@subgraph.com>

Permission is hereby granted, free of charge, to any person
obtaining a copy of this software and associated documentation
files (the "Software"), to deal in the Software without
restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following
conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
OTHER DEALINGS IN THE SOFTWARE.
"""

from distutils.core import setup

version = open('version').read().strip()

setup(name='Macouflage',
      version=version,
      description='Mac Spoofing Utility',
      author='David McKinney and Subgraph',
      author_email='info@subgraph.com',
      url='https://github.com/subgraph/macouflage/',
      platforms=['GNU/Linux'],
      license='GPL',

      scripts=['macouflage'],
      packages=['pymacouflage', 'pymacouflage.api', 'pymacouflage.gtkgui'],
      package_data={'pymacouflage.api': ['data/*.json']},
      data_files=[('/usr/share/applications', ['macouflage.desktop']),
       ]
     )
